package plugins.ylemontag.matlabexporter;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import plugins.ylemontag.matlabio.gui.DimensionMappingComponent;
import plugins.ylemontag.matlabio.gui.FileChooserComponent;
import plugins.ylemontag.matlabio.gui.SequenceButton;

public class MainPanel extends JPanel
{
	private static final long serialVersionUID = 1L;
	private FileChooserComponent _fileChooserComponent;
	private DimensionMappingComponent _dimensionMappingComponent;
	private VariableListComponent _variableListComponent;
	private SequenceButton _addSequenceButton;
	private JButton _removeDataButton;
	private JButton _removeAllButton;
	private JButton _exportButton;
	
	/**
	 * Matlab file chooser component
	 */
	public FileChooserComponent getFileComponent()
	{
		return _fileChooserComponent;
	}
	
	/**
	 * Dimension mapping component
	 */
	public DimensionMappingComponent getDimensionMappingComponent()
	{
		return _dimensionMappingComponent;
	}
	
	/**
	 * Variable list component
	 */
	public VariableListComponent getVariableComponent()
	{
		return _variableListComponent;
	}
	
	/**
	 * Add sequence button
	 */
	public SequenceButton getAddSequenceButton()
	{
		return _addSequenceButton;
	}
	
	/**
	 * Remove item button
	 */
	public JButton getRemoveDataButton()
	{
		return _removeDataButton;
	}
	
	/**
	 * Remove all items button
	 */
	public JButton getRemoveAllButton()
	{
		return _removeAllButton;
	}
	
	/**
	 * Remove all items button
	 */
	public JButton getExportButton()
	{
		return _exportButton;
	}
	
	/**
	 * Create the panel
	 */
	public MainPanel()
	{
		setBorder(new EmptyBorder(5, 5, 5, 5));
		setLayout(new BorderLayout(0, 0));
		
		_variableListComponent = new VariableListComponent();
		
		JScrollPane scrollPane = new JScrollPane(_variableListComponent);
		add(scrollPane, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		add(panel, BorderLayout.NORTH);
		panel.setLayout(new BorderLayout(0, 0));
		
		Component verticalStrut = Box.createVerticalStrut(5);
		panel.add(verticalStrut, BorderLayout.SOUTH);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel.add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		Component verticalStrut_1 = Box.createVerticalStrut(5);
		panel_1.add(verticalStrut_1, BorderLayout.NORTH);
		
		Component verticalStrut_2 = Box.createVerticalStrut(5);
		panel_1.add(verticalStrut_2, BorderLayout.SOUTH);
		
		Component horizontalStrut = Box.createHorizontalStrut(5);
		panel_1.add(horizontalStrut, BorderLayout.WEST);
		
		Component horizontalStrut_1 = Box.createHorizontalStrut(5);
		panel_1.add(horizontalStrut_1, BorderLayout.EAST);
		
		JPanel panel_2 = new JPanel();
		panel_1.add(panel_2, BorderLayout.CENTER);
		panel_2.setLayout(new BoxLayout(panel_2, BoxLayout.Y_AXIS));
		
		JPanel panel_7 = new JPanel();
		panel_2.add(panel_7);
		panel_7.setLayout(new BoxLayout(panel_7, BoxLayout.X_AXIS));
		
		JLabel lblNewLabel = new JLabel("Target file");
		panel_7.add(lblNewLabel);
		
		Component horizontalGlue = Box.createHorizontalGlue();
		panel_7.add(horizontalGlue);
		
		Component verticalStrut_3 = Box.createVerticalStrut(5);
		panel_2.add(verticalStrut_3);
		
		JPanel panel_6 = new JPanel();
		panel_2.add(panel_6);
		panel_6.setLayout(new BorderLayout(0, 0));
		
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Matlab object files (*.mat)", "mat");
		_fileChooserComponent = new FileChooserComponent(FileChooserComponent.Mode.SAVE_DIALOG, filter);
		panel_6.add(_fileChooserComponent, BorderLayout.CENTER);
		
		JPanel panel_3 = new JPanel();
		panel.add(panel_3, BorderLayout.EAST);
		panel_3.setLayout(new BoxLayout(panel_3, BoxLayout.X_AXIS));
		
		Component horizontalStrut_2 = Box.createHorizontalStrut(5);
		panel_3.add(horizontalStrut_2);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_3.add(panel_4);
		panel_4.setLayout(new BorderLayout(0, 0));
		
		Component verticalStrut_4 = Box.createVerticalStrut(5);
		panel_4.add(verticalStrut_4, BorderLayout.NORTH);
		
		Component verticalStrut_5 = Box.createVerticalStrut(5);
		panel_4.add(verticalStrut_5, BorderLayout.SOUTH);
		
		Component horizontalStrut_4 = Box.createHorizontalStrut(5);
		panel_4.add(horizontalStrut_4, BorderLayout.WEST);
		
		Component horizontalStrut_5 = Box.createHorizontalStrut(5);
		panel_4.add(horizontalStrut_5, BorderLayout.EAST);
		
		JPanel panel_5 = new JPanel();
		panel_4.add(panel_5, BorderLayout.CENTER);
		panel_5.setLayout(new BoxLayout(panel_5, BoxLayout.Y_AXIS));
		
		JPanel panel_11 = new JPanel();
		panel_5.add(panel_11);
		panel_11.setLayout(new BoxLayout(panel_11, BoxLayout.X_AXIS));
		
		JLabel lblNewLabel_1 = new JLabel("Dimension mapping");
		panel_11.add(lblNewLabel_1);
		
		Component horizontalGlue_2 = Box.createHorizontalGlue();
		panel_11.add(horizontalGlue_2);
		
		Component verticalStrut_6 = Box.createVerticalStrut(5);
		panel_5.add(verticalStrut_6);
		
		JPanel panel_10 = new JPanel();
		panel_5.add(panel_10);
		panel_10.setLayout(new BorderLayout(0, 0));
		
		_dimensionMappingComponent = new DimensionMappingComponent();
		panel_10.add(_dimensionMappingComponent, BorderLayout.CENTER);
		_dimensionMappingComponent.setToolTipText("Describe how a multi-dimension Matlab array should be interpreted as a sequence");
		
		JPanel panel_8 = new JPanel();
		add(panel_8, BorderLayout.SOUTH);
		panel_8.setLayout(new BorderLayout(0, 0));
		
		Component verticalStrut_7 = Box.createVerticalStrut(5);
		panel_8.add(verticalStrut_7, BorderLayout.NORTH);
		
		_exportButton = new JButton("Export");
		_exportButton.setToolTipText("Write the selected file with the listed objects");
		panel_8.add(_exportButton, BorderLayout.EAST);
		
		JPanel panel_9 = new JPanel();
		panel_8.add(panel_9, BorderLayout.WEST);
		panel_9.setLayout(new BoxLayout(panel_9, BoxLayout.X_AXIS));
		
		_addSequenceButton = new SequenceButton("Add sequence");
		_addSequenceButton.setToolTipText("Add an opened sequence to the list of objects to export");
		panel_9.add(_addSequenceButton);
		
		Component horizontalStrut_3 = Box.createHorizontalStrut(5);
		panel_9.add(horizontalStrut_3);
		
		_removeDataButton = new JButton("Remove");
		_removeDataButton.setToolTipText("Remove the selected items from the list of objects to export");
		panel_9.add(_removeDataButton);
		
		Component horizontalStrut_6 = Box.createHorizontalStrut(5);
		panel_9.add(horizontalStrut_6);
		
		_removeAllButton = new JButton("Remove all");
		_removeAllButton.setToolTipText("Clear the list of objects to export");
		panel_9.add(_removeAllButton);
		
		Component horizontalGlue_1 = Box.createHorizontalGlue();
		panel_8.add(horizontalGlue_1, BorderLayout.CENTER);
	}
}
