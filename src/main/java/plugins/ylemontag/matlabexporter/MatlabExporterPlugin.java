package plugins.ylemontag.matlabexporter;

import icy.gui.dialog.MessageDialog;
import icy.gui.frame.IcyFrame;
import icy.gui.frame.progress.ProgressFrame;
import icy.plugin.abstract_.PluginActionable;
import icy.preferences.XMLPreferences;
import icy.sequence.Sequence;
import icy.system.thread.ThreadUtil;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import plugins.ylemontag.matlabio.DimensionMapping;
import plugins.ylemontag.matlabio.MatlabExporter;
import plugins.ylemontag.matlabio.gui.DimensionMappingComponent;
import plugins.ylemontag.matlabio.gui.DimensionMappingComponent.DimensionMappingChangedListener;
import plugins.ylemontag.matlabio.gui.FileChooserComponent;
import plugins.ylemontag.matlabio.gui.FileChooserComponent.FileChangedListener;
import plugins.ylemontag.matlabio.gui.MatlabProgressFrame;
import plugins.ylemontag.matlabio.gui.SequenceButton;
import plugins.ylemontag.matlabio.gui.SequenceButton.SequenceButtonListener;
import plugins.ylemontag.matlabio.lib.Controller;
import plugins.ylemontag.matlabio.lib.MLIOException;
import plugins.ylemontag.matlabio.lib.MLMeta;

/**
 * 
 * @author Yoann Le Montagner
 *
 * Plugin to export sequences in .mat files
 */
public class MatlabExporterPlugin extends PluginActionable
{
	private FileChooserComponent _fileChooserComponent;
	private DimensionMappingComponent _dimensionMappingComponent;
	private VariableListComponent _variableListComponent;
	private SequenceButton _addSequenceButton;
	private JButton _removeDataButton;
	private JButton _removeAllButton;
	private JButton _exportButton;
	private File _file;
	private MatlabExporter _exporter;
	private boolean _append;
	private boolean _currentlyExporting;
	
	@Override
	public void run()
	{
		// Non-GUI initializations
		_currentlyExporting = false;
		
		// GUI
		IcyFrame frame = new IcyFrame("Matlab exporter", true, true, true, true);
		frame.setSize        (500, 400);
		frame.setSizeExternal(500, 400);
		MainPanel mainPanel = new MainPanel();
		frame.add(mainPanel);
		addIcyFrame(frame);
		frame.center();
		frame.setVisible(true);
		frame.requestFocus();
		_fileChooserComponent      = mainPanel.getFileComponent            ();
		_dimensionMappingComponent = mainPanel.getDimensionMappingComponent();
		_variableListComponent     = mainPanel.getVariableComponent        ();
		_addSequenceButton         = mainPanel.getAddSequenceButton        ();
		_removeDataButton          = mainPanel.getRemoveDataButton         ();
		_removeAllButton           = mainPanel.getRemoveAllButton          ();
		_exportButton              = mainPanel.getExportButton             ();
		retrievePersistentDimensionMapping();
		retrievePersistentDirectory();
		refreshActionButtons();
		
		// Listeners
		_fileChooserComponent.addFileChangedListener(new FileChangedListener()
		{	
			@Override
			public void actionPerformed(File newFile) {
				onTargetFileChanged();
			}
		});
		_dimensionMappingComponent.addDimensionMappingChangedListener(new DimensionMappingChangedListener()
		{
			@Override
			public void actionPerformed() {
				onDimensionMappingChanged();
			}
		});
		_variableListComponent.getSelectionModel().addListSelectionListener(new ListSelectionListener()
		{
			@Override
			public void valueChanged(ListSelectionEvent e) {
				refreshActionButtons();
			}
		});
		_variableListComponent.addNameEditListener(new VariableListComponent.NameEditListener()
		{
			@Override
			public void nameChanged(String oldName, String newName) {
				updateVariableName(oldName, newName);
			}
		});
		_variableListComponent.addComplexEditListener(new VariableListComponent.ComplexEditListener()
		{
			@Override
			public void isComplexChanged(String name, boolean newIsComplex) {
				updateVariableComplexFlag(name, newIsComplex);
			}
		});
		_addSequenceButton.addSequenceButtonListener(new SequenceButtonListener()
		{	
			@Override
			public void eventTriggered(Sequence seq) {
				addSequenceToMatFile(seq);
			}
		});
		_removeDataButton.addActionListener(new ActionListener()
		{	
			@Override
			public void actionPerformed(ActionEvent e) {
				removeSelectedItems();
			}
		});
		_removeAllButton.addActionListener(new ActionListener()
		{	
			@Override
			public void actionPerformed(ActionEvent e) {
				removeAllItems();
			}
		});
		_exportButton.addActionListener(new ActionListener()
		{	
			@Override
			public void actionPerformed(ActionEvent e) {
				executeExport();
			}
		});
	}
	
	/**
	 * Action performed when the user changes target file
	 */
	private void onTargetFileChanged()
	{
		updatePersistentDirectory();
		_file = _fileChooserComponent.getSelectedFile();
		_append = _file!=null && _file.exists();
		rebuildExporter();
	}
	
	/**
	 * Rebuild the exporter
	 */
	private void rebuildExporter()
	{
		// No exporter if no file is selected
		if(_file==null) {
			_exporter = null;
		}
		
		// Otherwise
		else {
			try
			{
				// Re-allocate a new exporter, and copy the currently pending variables
				MatlabExporter newExporter = new MatlabExporter(_file, _append);
				if(_exporter!=null) {
					newExporter.putPendingData(_exporter);
				}
				_exporter = newExporter;
			}
			catch(IOException err) {
				String message = "Cannot create a Matlab file at " + _file.getName() + ": " + err;
				MessageDialog.showDialog("Error", message, MessageDialog.ERROR_MESSAGE);
				_fileChooserComponent.setSelectedFile(null);
				return;
			}
		}
		
		// Refresh the components
		refreshVariableList ();
		refreshActionButtons();
	}
	
	/**
	 * Action performed when the user changes the dimension mapping
	 */
	private void onDimensionMappingChanged()
	{
		if(_exporter==null) {
			return;
		}
		try {
			DimensionMapping newMapping = _dimensionMappingComponent.getDimensionMapping();
			for(String v : _exporter.getPendingVariables()) {
				_exporter.updateDimensionMapping(v, newMapping);
			}
		}
		catch (MLIOException err) {
			String message = "Cannot change the dimension mapping: " + err;
			MessageDialog.showDialog("Error", message, MessageDialog.ERROR_MESSAGE);
		}
		refreshVariableList();
		updatePersistentDimensionMapping();
	}
	
	/**
	 * Change the name of a variable
	 */
	private void updateVariableName(String oldName, String newName)
	{
		if(_exporter==null) {
			return;
		}
		try {
			_exporter.updateName(oldName, newName);
		}
		catch (MLIOException err) {
			String message = "Cannot rename the variable " + oldName + " into " + newName + ": " + err;
			MessageDialog.showDialog("Error", message, MessageDialog.ERROR_MESSAGE);
		}
		refreshVariableList();
	}
	
	/**
	 * Change the complex flag of a variable
	 */
	private void updateVariableComplexFlag(String name, boolean newIsComplex)
	{
		if(_exporter==null) {
			return;
		}
		try {
			_exporter.updateIsComplex(name, newIsComplex);
		}
		catch (MLIOException err) {
			String message = "Cannot change the complex flag of " + name + ": " + err;
			MessageDialog.showDialog("Error", message, MessageDialog.ERROR_MESSAGE);
		}
		refreshVariableList();
	}
	
	/**
	 * Add the given sequence to the list of objects to export
	 */
	private void addSequenceToMatFile(Sequence seq)
	{
		if(_exporter==null) {
			return;
		}
		try {
			_exporter.putData(seq, seq.getName(), _dimensionMappingComponent.getDimensionMapping());
		}
		catch (MLIOException err) {
			String message = "Cannot export the sequence " + seq + ": " + err;
			MessageDialog.showDialog("Error", message, MessageDialog.ERROR_MESSAGE);
		}
		refreshVariableList ();
		refreshActionButtons();
	}
	
	/**
	 * Remove the selected items from the list of objects to export
	 */
	private void removeSelectedItems()
	{
		if(_exporter==null) {
			return;
		}
		final List<String> names = _variableListComponent.getSelectedUnsavedVariables();
		try {
			for(final String name : names) {
				_exporter.eraseData(name);
			}
		}
		catch (MLIOException err) {
			String message = "Cannot remove the selected objects from the file: " + err;
			MessageDialog.showDialog("Error", message, MessageDialog.ERROR_MESSAGE);
		}
		refreshVariableList ();
		refreshActionButtons();
	}
	
	/**
	 * Remove all the items from the list of objects to export
	 */
	private void removeAllItems()
	{
		if(_exporter==null) {
			return;
		}
		try {
			_exporter.eraseAll();
		}
		catch (MLIOException err) {
			String message = "Cannot remove the objects from the file: " + err;
			MessageDialog.showDialog("Error", message, MessageDialog.ERROR_MESSAGE);
		}
		refreshVariableList ();
		refreshActionButtons();
	}
	
	/**
	 * Execute the export operation
	 */
	private void executeExport()
	{
		if(_exporter==null || _file==null) {
			return;
		}
		freezeComponents(true);
		final Controller    controller    = new Controller();
		final ProgressFrame progressFrame = new MatlabProgressFrame(
			"Exporting file " + _file.getName(), controller
		);
		ThreadUtil.bgRun(new Runnable()
		{
			@Override
			public void run() {
				try {
					_exporter.export(controller);
				}
				catch (final IOException err) {
					ThreadUtil.invokeLater(new Runnable()
					{	
						@Override
						public void run() {
							String message = "Error while exporting the file " + _file.getName() + ": " + err;
							MessageDialog.showDialog("Error", message, MessageDialog.ERROR_MESSAGE);
						}
					});
				}
				catch(Controller.CanceledByUser err) {}
				finally {
					ThreadUtil.invokeLater(new Runnable()
					{
						@Override
						public void run() {
							progressFrame.close();
							freezeComponents(false);
							refreshVariableList();
						}
					});
				}
			}
		});
	}
	
	/**
	 * Refresh the component that display the list of variables to export in
	 * the currently selected .mat file
	 */
	private void refreshVariableList()
	{
		// Retrieve the current selection
		List<String> currentSelection = _variableListComponent.getSelectedVariables();
		
		// Update the list of items
		_variableListComponent.clearRows();
		if(_exporter==null) {
			return;
		}
		Set<String> names = _exporter.getAllVariables();
		for(String name : names) {
			MLMeta  meta  = _exporter.getMeta(name);
			boolean saved = !_exporter.isPendingVariable(name);
			_variableListComponent.addRow(meta, saved);
		}
		
		// Enforce the previous selection
		_variableListComponent.setSelectedVariables(currentSelection);
	}
	
	/**
	 * Refresh the activation state of the remove buttons
	 */
	private void refreshActionButtons()
	{
		boolean precondition     = (_exporter!=null && !_currentlyExporting);
		boolean addEnabled       = precondition;
		boolean removeSelEnabled = (precondition && _variableListComponent.getSelectedUnsavedVariables().size()!=0);
		boolean removeAllEnabled = (precondition && _exporter.getPendingVariables().size()!=0);
		boolean exportEnabled    = (precondition && !isUpToDate());
		_addSequenceButton.setMode(addEnabled ? SequenceButton.Mode.AUTOMATIC : SequenceButton.Mode.ALWAYS_DISABLED);
		_removeDataButton.setEnabled(removeSelEnabled);
		_removeAllButton .setEnabled(removeAllEnabled);
		_exportButton    .setEnabled(exportEnabled   );
	}
	
	/**
	 * All the component are disabled during an export operation
	 */
	private void freezeComponents(boolean freeze)
	{
		_currentlyExporting = freeze;
		refreshActionButtons();
		_fileChooserComponent     .setEnabled(!_currentlyExporting);
		_dimensionMappingComponent.setEnabled(!_currentlyExporting);
		_variableListComponent    .setEnabled(!_currentlyExporting);
	}
	
	/**
	 * Is the file up to date?
	 */
	private boolean isUpToDate()
	{
		return _exporter==null || _exporter.getPendingVariables().isEmpty();
	}
	
	/**
	 * Retrieve the persistent dimension mapping parameters
	 */
	private void retrievePersistentDimensionMapping()
	{
		XMLPreferences prefs = getPreferences("DimensionMapping");
		DimensionMapping mapping = _dimensionMappingComponent.getDimensionMapping();
		int dimX = mapping.getDimensionX();
		int dimY = mapping.getDimensionY();
		int dimZ = mapping.getDimensionZ();
		int dimT = mapping.getDimensionT();
		int dimC = mapping.getDimensionC();
		dimX = prefs.getInt("DimX", dimX);
		dimY = prefs.getInt("DimY", dimY);
		dimZ = prefs.getInt("DimZ", dimZ);
		dimT = prefs.getInt("DimT", dimT);
		dimC = prefs.getInt("DimC", dimC);
		_dimensionMappingComponent.setDimensions(dimX, dimY, dimZ, dimT, dimC);
	}
	
	/**
	 * Update the persistent dimension mapping parameters
	 */
	private void updatePersistentDimensionMapping()
	{
		XMLPreferences prefs = getPreferences("DimensionMapping");
		DimensionMapping mapping = _dimensionMappingComponent.getDimensionMapping();
		prefs.putInt("DimX", mapping.getDimensionX());
		prefs.putInt("DimY", mapping.getDimensionY());
		prefs.putInt("DimZ", mapping.getDimensionZ());
		prefs.putInt("DimT", mapping.getDimensionT());
		prefs.putInt("DimC", mapping.getDimensionC());
	}
	
	/**
	 * Retrieve the persistent directory
	 */
	private void retrievePersistentDirectory()
	{
		XMLPreferences prefs = getPreferences("Directory");
		String directoryPath = _fileChooserComponent.getCurrentDirectory().getAbsolutePath();
		directoryPath = prefs.get("Path", directoryPath);
		_fileChooserComponent.setCurrentDirectory(new File(directoryPath));
	}
	
	/**
	 * Update the persistent directory
	 */
	private void updatePersistentDirectory()
	{
		XMLPreferences prefs = getPreferences("Directory");
		prefs.put("Path", _fileChooserComponent.getCurrentDirectory().getAbsolutePath());
	}
}
