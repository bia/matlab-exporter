package plugins.ylemontag.matlabexporter;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import plugins.ylemontag.matlabio.lib.MLMeta;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Component used to display a list of Matlab objects contained in a .mat file
 */
public class VariableListComponent extends JTable
{
	private static final long serialVersionUID = 1L;
	
	/**
	 * Listener for the name field edition events
	 */
	public interface NameEditListener
	{
		/**
		 * Method fired
		 */
		public void nameChanged(String oldName, String newName);
	}
	
	/**
	 * Listener for the complex field edition events
	 */
	public interface ComplexEditListener
	{
		/**
		 * Method fired
		 */
		public void isComplexChanged(String name, boolean newIsComplex);
	}
	
	private List<NameEditListener> _nameEditListeners;
	private List<ComplexEditListener> _complexEditListeners;
	private HashMap<Integer, String> _index;
	private DefaultTableModel _model;
	
	/**
	 * Constructor
	 */
	public VariableListComponent()
	{
		super();
		_nameEditListeners = new LinkedList<NameEditListener>();
		_complexEditListeners = new LinkedList<ComplexEditListener>();
		_index = new HashMap<Integer, String>();
		_model = new Model();
		_model.addTableModelListener(new TableModelListener()
		{
			@Override
			public void tableChanged(TableModelEvent e) {
				if(e.getType()!=TableModelEvent.UPDATE) {
					return;
				}
				int row = e.getFirstRow();
				String name = _index.get(row);
				if(name==null) {
					return;
				}
				if(e.getColumn()==1) {
					String newName = (String)getValueAt(row, 1);
					_index.put(row, newName);
					fireNameEditListeners(name, newName);
				}
				else if(e.getColumn()==3) {
					boolean newIsComplex = (Boolean)getValueAt(row, 3);
					fireComplexEditListeners(name, newIsComplex);
				}
			}
		});
		setModel(_model);
		TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(_model);
		setRowSorter(sorter);
	}
	
	/**
	 * Add a new name edit listener
	 */
	public void addNameEditListener(NameEditListener l)
	{
		_nameEditListeners.add(l);
	}
	
	/**
	 * Add a new complex edit listener
	 */
	public void addComplexEditListener(ComplexEditListener l)
	{
		_complexEditListeners.add(l);
	}
	
	/**
	 * Return the variable name corresponding to a given row. The row index is
	 * relative to the JTable (not to the model)
	 */
	public String getVariableAt(int rowIndex)
	{
		return (String)getValueAt(rowIndex, 1);
	}
	
	/**
	 * Return the list of selected variables
	 */
	public List<String> getSelectedVariables()
	{
		int[] selectedRows = getSelectedRows();
		List<String> retVal = new LinkedList<String>();
		for(int selectedRow : selectedRows) {
			retVal.add(getVariableAt(selectedRow));
		}
		return retVal;
	}
	
	/**
	 * Return the list of both selected and non-saved variables
	 */
	public List<String> getSelectedUnsavedVariables()
	{
		int[] selectedRows = getSelectedRows();
		List<String> retVal = new LinkedList<String>();
		for(int selectedRow : selectedRows) {
			if(!(Boolean)getValueAt(selectedRow, 0)) {
				retVal.add(getVariableAt(selectedRow));
			}
		}
		return retVal;
	}
	
	/**
	 * Set the list of selected variables
	 */
	public void setSelectedVariables(List<String> selectedVariables)
	{
		clearSelection();
		for(int r=0; r<getRowCount(); ++r) {
			if(selectedVariables.contains(getVariableAt(r))) {
				getSelectionModel().addSelectionInterval(r, r);
			}
		}
	}
	
	/**
	 * Add a row the table
	 */
	public void addRow(MLMeta meta, boolean saved)
	{
		String  name = meta.getName();
		String  dims = meta.getDimensionsAsString();
		Boolean cplx = meta.getIsComplex();
		String  type = meta.getType().toString();
		_index.put(_index.size(), name);
		_model.addRow(new Object[] {saved, name, type, cplx, dims});
	}
	
	/**
	 * Remove all the rows
	 */
	public void clearRows()
	{
		_model.setRowCount(0);
		_index.clear();
	}
	
	/**
	 * Fire the name edit listeners
	 */
	private void fireNameEditListeners(String oldName, String newName)
	{
		for(NameEditListener l : _nameEditListeners) {
			l.nameChanged(oldName, newName);
		}
	}
	
	/**
	 * Fire the complex edit listeners
	 */
	private void fireComplexEditListeners(String name, boolean newIsComplex)
	{
		for(ComplexEditListener l : _complexEditListeners) {
			l.isComplexChanged(name, newIsComplex);
		}
	}
	
	/**
	 * Model used in the component
	 */
	private static class Model extends DefaultTableModel
	{
		private static final long serialVersionUID = 1L;
		
		/**
		 * Constructor
		 */
		public Model()
		{
			super();
			addColumn("Saved"     );
			addColumn("Variable"  );
			addColumn("Type"      );
			addColumn("Complex"   );
			addColumn("Dimensions");
		}
		
		@Override
		public boolean isCellEditable(int row, int column)
		{
			return (column==1 || column==3) && ((Boolean)getValueAt(row, 0)==false);
		}
		
		@Override
		public int getColumnCount()
		{
			return 5;
		}
		
		@Override
		public Class<?> getColumnClass(int columnIndex)
		{
			return (columnIndex==0 || columnIndex==3) ? Boolean.class : String.class;
		}
	}
}
